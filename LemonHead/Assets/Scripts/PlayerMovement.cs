﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.GlobalIllumination;

public class PlayerMovement : MonoBehaviour
{
    Animator m_Animator;
    Rigidbody m_Rigidbody;
    AudioSource m_AudioSource;
    Vector3 m_Movement;
    Quaternion m_Rotation = Quaternion.identity;
    public float turnSpeed = 20f;

    // Start is called before the first frame update

    bool biggering;
    bool smalling;



    void Start()
    {
        m_Animator = GetComponent<Animator>();
        m_Rigidbody = GetComponent<Rigidbody>();
        m_AudioSource = GetComponent<AudioSource>();
      
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        float horizontal = Input.GetAxis("Horizontal");
        float vertical = Input.GetAxis("Vertical");
        


        m_Movement.Set(horizontal, 0f, vertical);
        m_Movement.Normalize();

        bool hasHorizontalInput = !Mathf.Approximately(horizontal, 0f);
        bool hasVerticalInput = !Mathf.Approximately(vertical, 0f);
        bool isWalking = hasHorizontalInput || hasVerticalInput;
        m_Animator.SetBool("IsWalking", isWalking);

        if (isWalking)
        {
            if (!m_AudioSource.isPlaying)
            {
                m_AudioSource.Play();
            }
        }
        else
        {
            m_AudioSource.Stop();
        }

        Vector3 desiredForward = Vector3.RotateTowards(transform.forward, m_Movement, turnSpeed * Time.deltaTime, 0f);
        m_Rotation = Quaternion.LookRotation(desiredForward);


    }


    private void Update()
    {
        bool Big = Input.GetKeyDown(KeyCode.P);
        bool antibig = Input.GetKeyUp(KeyCode.P);

        bool Shrink = Input.GetKeyDown(KeyCode.O);
        bool antiShrink = Input.GetKeyUp(KeyCode.O);


        //Making Lemon Grow
        while (!biggering && Big)
        {
            biggering = true;
            gameObject.transform.localScale += new Vector3(.5f, .5f, .5f);
        }
        while (biggering && antibig)
        {
            biggering = false;
            gameObject.transform.localScale += new Vector3(-.5f, -.5f, -.5f);
        }


        //Making Lemon Shrink
        while (!smalling && Shrink)
        {
            smalling = true;
            gameObject.transform.localScale += new Vector3(-.5f, -.5f, -.5f);
        }
        while (smalling && antiShrink)
        {
            smalling = false;
            gameObject.transform.localScale += new Vector3(.5f, .5f, .5f);
        }

    }


    void OnAnimatorMove()
    {
        m_Rigidbody.MovePosition(m_Rigidbody.position + m_Movement * m_Animator.deltaPosition.magnitude);
        m_Rigidbody.MoveRotation(m_Rotation);
    }



}
