﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPC : MonoBehaviour
{
    public string NPCname;
    public Dialogue dialog_Manager;
    public List<string> npcConvo = new List<string>();

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            dialog_Manager.StartDialogue(NPCname, npcConvo);
        }
    }


}
