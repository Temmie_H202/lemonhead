﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Dialogue : MonoBehaviour
{

    public GameObject dialogPanel;
    public Text npcnametext;
    public Text dialogtext;

    private List<string> conversation;
    private int convoIndex;


    private void Start()
    {
        dialogPanel.SetActive(false);
    }


    public void StartDialogue(string _npcName, List<string> _convo)
    {
        npcnametext.text = _npcName;
        conversation = new List<string>(_convo);
        dialogPanel.SetActive(true);
        convoIndex = 0;
        ShowText();
    }

    public void StopDialogue()
    {
        dialogPanel.SetActive(false);
    }

    private void ShowText()
    {
        dialogtext.text = conversation[convoIndex];
    }

    public void Next()
    {
        Debug.Log("hi");
        if (convoIndex < conversation.Count - 1)
        {
            Debug.Log("hello");
            convoIndex += 1;
            ShowText();
            Debug.Log("hey");
        }
        
    }


}
